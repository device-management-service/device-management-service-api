module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
  collectCoverageFrom: ['<rootDir>/modules/**/*.ts', '<rootDir>/main/**/*.ts', '!<rootDir>/**/(*.module).ts'],
  moduleNameMapper: {
    '^@config/(.*)$': '<rootDir>/config/$1',
    '^@util/(.*)$': '<rootDir>/util/$1',
    '^@modules/(.*)$': '<rootDir>/modules/$1',
    '^@schema/(.*)$': '<rootDir>/schema/$1',
  },
};
