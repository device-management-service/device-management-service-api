import { Injectable, Logger } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { CreateUserDtoRequest } from './dto/create-user.dto';
import { UserDal } from './user.dal';
import { HttpResponse } from '@util/http-response';
import responseConfig from '@config/response.config';
import { hashSalt } from '@config/constant';
import { User } from 'user.type';

@Injectable()
export class UserService {
  private readonly logger: Logger = new Logger(UserService.name);
  constructor(private userDal: UserDal) {}

  async create(createUserDto: CreateUserDtoRequest): Promise<HttpResponse<void>> {
    this.logger.log('user.create');
    this.logger.log({ username: createUserDto, email: createUserDto.email });

    const { email, password } = createUserDto;

    const existingUser = await this.userDal.findOne(email);
    if (existingUser) {
      return {
        statusCode: responseConfig.USER_ALREADY_EXIST.statusCode,
        message: responseConfig.USER_ALREADY_EXIST.message,
      };
    }

    const hashedPassword = await bcrypt.hash(password, hashSalt);

    try {
      await this.userDal.create({
        ...createUserDto,
        password: hashedPassword,
        createdAt: new Date(),
      });
      return {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };
    } catch (err) {
      this.logger.error('create user error');
      this.logger.error(err);
      return {
        statusCode: responseConfig.CANNOT_CREATE_USER.statusCode,
        message: responseConfig.CANNOT_CREATE_USER.message,
      };
    }
  }

  async findOne(email: string): Promise<User> {
    const user = await this.userDal.findOne(email);
    return user;
  }
}
