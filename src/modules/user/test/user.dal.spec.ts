import { Test } from '@nestjs/testing';
import { UserDal } from '../user.dal';
import { User } from 'user.type';
import { UserEntity } from '@schema/user.schema';
import { getModelToken } from '@nestjs/mongoose';

describe('UserDal', () => {
  let userDal: UserDal;

  class MockUserModel {
    constructor(private readonly userData: User) {}
    save = jest.fn().mockResolvedValue(this.userData);
    static findOne = jest.fn().mockImplementation(() => ({
      exec: jest.fn().mockResolvedValue(user),
    }));
  }

  const user: User = {
    username: 'Test User',
    email: 'test@example.com',
    password: 'hashedPassword',
    createdAt: new Date(),
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UserDal,
        {
          provide: getModelToken(UserEntity.name),
          useValue: MockUserModel,
        },
      ],
    }).compile();

    userDal = moduleRef.get<UserDal>(UserDal);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('create', () => {
    it('should successfully create a user', async () => {
      const result = await userDal.create(user);

      expect(result).toBe(true);
    });
  });

  describe('findOne', () => {
    it('should return a user', async () => {
      const email = user.email;
      const result = await userDal.findOne(email);

      expect(result).toEqual(user);
    });
  });
});
