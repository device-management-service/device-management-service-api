import { Test } from '@nestjs/testing';
import { UserService } from '../user.service';
import { UserDal } from '../user.dal';
import * as bcrypt from 'bcryptjs';
import { CreateUserDtoRequest } from '../dto/create-user.dto';
import responseConfig from '@config/response.config';

describe('UserService', () => {
  let userService: UserService;
  let userDal: UserDal;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: UserDal,
          useValue: {
            create: jest.fn(),
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userDal = moduleRef.get<UserDal>(UserDal);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('create', () => {
    it('should create a user if not already existing', async () => {
      const createUserDto: CreateUserDtoRequest = {
        username: 'Test User',
        email: 'test@example.com',
        password: 'password123',
      };

      jest.spyOn(userDal, 'findOne').mockResolvedValue(null);
      jest.spyOn(userDal, 'create').mockResolvedValue(true);
      jest.spyOn(bcrypt, 'hash').mockResolvedValue('hashedPassword' as never);

      const result = await userService.create(createUserDto);

      expect(userDal.findOne).toHaveBeenCalledWith(createUserDto.email);
      expect(bcrypt.hash).toHaveBeenCalledWith(createUserDto.password, 10);
      expect(userDal.create).toHaveBeenCalledWith({
        ...createUserDto,
        password: 'hashedPassword',
        createdAt: expect.any(Date),
      });
      expect(result).toEqual({
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      });
    });

    it('should return an error if user already exists', async () => {
      const createdAt = new Date('2023-01-01');
      const createUserDto: CreateUserDtoRequest = {
        username: 'Test User',
        email: 'test@example.com',
        password: 'password123',
      };

      jest.spyOn(userDal, 'findOne').mockResolvedValue({ ...createUserDto, createdAt });

      const result = await userService.create(createUserDto);

      expect(userDal.findOne).toHaveBeenCalledWith(createUserDto.email);
      expect(result).toEqual({
        statusCode: responseConfig.USER_ALREADY_EXIST.statusCode,
        message: responseConfig.USER_ALREADY_EXIST.message,
      });
    });

    it('should handle errors while creating user', async () => {
      const createUserDto: CreateUserDtoRequest = {
        username: 'Test User',
        email: 'test@example.com',
        password: 'password123',
      };

      jest.spyOn(userDal, 'findOne').mockResolvedValue(null);
      jest.spyOn(userDal, 'create').mockRejectedValue(new Error('Create User Error'));

      const result = await userService.create(createUserDto);

      expect(userDal.findOne).toHaveBeenCalledWith(createUserDto.email);
      expect(result).toEqual({
        statusCode: responseConfig.CANNOT_CREATE_USER.statusCode,
        message: responseConfig.CANNOT_CREATE_USER.message,
      });
    });
  });

  describe('findOne', () => {
    it('should return a user if existing', async () => {
      const user = {
        username: 'Test User',
        email: 'test@example.com',
        password: 'password123',
        createdAt: new Date(),
      };

      jest.spyOn(userDal, 'findOne').mockResolvedValue(user);

      const result = await userService.findOne(user.email);

      expect(userDal.findOne).toHaveBeenCalledWith(user.email);
      expect(result).toEqual(user);
    });

    it('should return null if user does not exist', async () => {
      const email = 'nonexistent@example.com';

      jest.spyOn(userDal, 'findOne').mockResolvedValue(null);

      const result = await userService.findOne(email);

      expect(userDal.findOne).toHaveBeenCalledWith(email);
      expect(result).toBeNull();
    });
  });
});
