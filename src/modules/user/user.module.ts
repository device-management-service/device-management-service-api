import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from './user.service';
import { UserDal } from './user.dal';
import { UserEntity, UserSchema } from '@schema/user.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: UserEntity.name, schema: UserSchema }])],
  providers: [UserService, UserDal],
  exports: [UserService],
})
export class UserModule {}
