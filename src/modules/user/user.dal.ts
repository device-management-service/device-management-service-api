import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'user.type';
import { UserEntity, UserDocument } from '@schema/user.schema';

@Injectable()
export class UserDal {
  constructor(@InjectModel(UserEntity.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: User): Promise<boolean> {
    const createdUser = new this.userModel(createUserDto);
    const result = await createdUser.save();
    return !!result;
  }

  async findOne(email: string): Promise<User> {
    return this.userModel.findOne({ email }).exec();
  }
}
