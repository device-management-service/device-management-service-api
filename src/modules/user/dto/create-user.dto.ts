export interface CreateUserDtoRequest {
  username: string;
  email: string;
  password: string;
}
