import { Controller, Body, Version, Request, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDtoRequest } from '@modules/user/dto/create-user.dto';
import { HttpResponse } from '@util/http-response';
import { AuthGuard } from '@nestjs/passport';
import { ApiOperation, ApiResponse, ApiBody, ApiTags } from '@nestjs/swagger';
import { RegisterRequestDocument, RegisterResponseDocument } from './document/register.document';
import { LoginRequestDocument, LoginResponseDocument } from './document/login.document';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Version('1')
  @Post('register')
  @ApiOperation({ summary: 'Register a new user' })
  @ApiBody({ type: RegisterRequestDocument })
  @ApiResponse({
    status: 201,
    description: 'Successfully Registered',
    type: RegisterResponseDocument,
  })
  @Version('1')
  @Post('register')
  async register(@Body() createUserDtoRequest: CreateUserDtoRequest): Promise<HttpResponse<void>> {
    return this.authService.register(createUserDtoRequest);
  }

  @Version('1')
  @UseGuards(AuthGuard('local'))
  @Post('login')
  @ApiOperation({ summary: 'Login user' })
  @ApiBody({ type: LoginRequestDocument })
  @ApiResponse({
    status: 200,
    description: 'Successful Login',
    type: LoginResponseDocument,
  })
  async login(@Request() { user }) {
    return this.authService.login(user);
  }
}
