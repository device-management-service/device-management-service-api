import { ApiProperty } from '@nestjs/swagger';
import { LoginDtoResponse } from '../dto/login.dto';

export class LoginRequestDocument {
  @ApiProperty({
    name: 'email',
    example: 'user@mail.com',
  })
  email: string;

  @ApiProperty({
    name: 'password',
    example: 'password',
  })
  password: string;
}

export class LoginResponseDocument {
  @ApiProperty({
    name: 'statusCode',
    example: 1,
  })
  statusCode: number;
  @ApiProperty({
    name: 'message',
    example: 'Success',
  })
  message: string;
  @ApiProperty({
    name: 'data',
    example: {
      accessToken: 'accessToken',
      username: 'user@mail.com',
    },
  })
  data: LoginDtoResponse;
}
