import { ApiProperty } from '@nestjs/swagger';
import { CreateUserDtoRequest } from '@modules/user/dto/create-user.dto';

export class RegisterRequestDocument implements CreateUserDtoRequest {
  @ApiProperty({
    name: 'username',
    example: 'user1',
  })
  username: string;

  @ApiProperty({
    name: 'email',
    example: 'user@mail.com',
  })
  email: string;

  @ApiProperty({
    name: 'password',
    example: 'password',
  })
  password: string;
}

export class RegisterResponseDocument {
  @ApiProperty({
    name: 'statusCode',
    example: 1,
  })
  statusCode: number;
  @ApiProperty({
    name: 'message',
    example: 'Success',
  })
  message: string;
}
