import { Test, TestingModule } from '@nestjs/testing';
import { UnauthorizedException } from '@nestjs/common';
import { JwtStrategy } from '../jwt.strategy';
import { UserService } from '@modules/user/user.service';
import { User } from 'user.type';

describe('JwtStrategy', () => {
  let jwtStrategy: JwtStrategy;
  let userServiceMock: any;

  const mockUser: User = {
    username: 'Test User',
    email: 'test@example.com',
    password: 'hashedPassword',
    createdAt: new Date(),
  };

  beforeEach(async () => {
    process.env.APP_SECRET = 'test_secret';
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        {
          provide: UserService,
          useValue: {
            findOne: jest.fn().mockResolvedValue(mockUser),
          },
        },
      ],
    }).compile();

    jwtStrategy = module.get<JwtStrategy>(JwtStrategy);
    userServiceMock = module.get<UserService>(UserService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('validate', () => {
    it('should validate and return the user based on JWT payload', async () => {
      const user = await jwtStrategy.validate({ username: mockUser.username, email: mockUser.email });

      expect(userServiceMock.findOne).toHaveBeenCalledWith(mockUser.email);
      expect(user).toEqual({ username: mockUser.username, email: mockUser.email });
    });

    it('should throw an error when user does not exist', async () => {
      userServiceMock.findOne.mockResolvedValue(null);

      try {
        await jwtStrategy.validate({ username: mockUser.username, email: mockUser.email });
      } catch (error) {
        expect(error).toBeInstanceOf(UnauthorizedException);
        expect(userServiceMock.findOne).toHaveBeenCalledWith(mockUser.email);
      }
    });
  });
});
