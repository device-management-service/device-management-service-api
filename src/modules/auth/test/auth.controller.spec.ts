import { Test } from '@nestjs/testing';
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import responseConfig from '@config/response.config';
import { CreateUserDtoRequest } from '@modules/user/dto/create-user.dto';
import { User } from 'user.type';

describe('AuthController', () => {
  let authController: AuthController;
  let authServiceMock: any;

  const mockUser: User = {
    username: 'Test User',
    email: 'test@example.com',
    password: 'hashedPassword',
    createdAt: new Date(),
  };

  const mockRequest = {
    user: mockUser,
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn().mockResolvedValue({
              statusCode: responseConfig.SUCCESS.statusCode,
              message: responseConfig.SUCCESS.message,
              data: { username: mockUser.username, accessToken: 'JWT_TOKEN' },
            }),
            register: jest.fn().mockResolvedValue({
              statusCode: responseConfig.SUCCESS.statusCode,
              message: responseConfig.SUCCESS.message,
            }),
          },
        },
      ],
    }).compile();

    authController = moduleRef.get<AuthController>(AuthController);
    authServiceMock = moduleRef.get<AuthService>(AuthService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('login', () => {
    it('should return an access token', async () => {
      const result = await authController.login(mockRequest);

      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
        data: { username: mockUser.username, accessToken: 'JWT_TOKEN' },
      };

      expect(authServiceMock.login).toHaveBeenCalledWith(mockUser);
      expect(result).toEqual(response);
    });
  });

  describe('register', () => {
    it('should register a user', async () => {
      const createUserDto: CreateUserDtoRequest = {
        username: mockUser.username,
        email: mockUser.email,
        password: mockUser.password,
      };

      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };

      const result = await authController.register(createUserDto);

      expect(authServiceMock.register).toHaveBeenCalledWith(createUserDto);
      expect(result).toEqual(response);
    });
  });
});
