import { Test } from '@nestjs/testing';
import { AuthService } from '../auth.service';
import { UserService } from '@modules/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { User } from 'user.type';
import * as bcrypt from 'bcryptjs';

describe('AuthService', () => {
  let authService: AuthService;
  let userServiceMock: any;

  const mockUser: User = {
    username: 'Test User',
    email: 'test@example.com',
    password: 'hashedPassword',
    createdAt: new Date(),
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UserService,
          useValue: {
            findOne: jest.fn().mockResolvedValue(mockUser),
            create: jest.fn().mockResolvedValue(true),
          },
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn().mockReturnValue('JWT_TOKEN'),
          },
        },
      ],
    }).compile();

    authService = moduleRef.get<AuthService>(AuthService);
    userServiceMock = moduleRef.get<UserService>(UserService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('validateUser', () => {
    it('should return user info if credentials are valid', async () => {
      const passwordSpy = jest.spyOn(bcrypt, 'compare').mockResolvedValue(true as never);

      const result = await authService.validateUser(mockUser.email, mockUser.password);

      expect(result).toEqual({
        username: mockUser.username,
        email: mockUser.email,
      });

      passwordSpy.mockRestore();
    });

    it('should return null if user does not exist', async () => {
      userServiceMock.findOne.mockResolvedValue(null);
      const result = await authService.validateUser('nonexistent@example.com', '123456');

      expect(result).toBeNull();
    });

    it('should return null if password is incorrect', async () => {
      const passwordSpy = jest.spyOn(bcrypt, 'compare').mockResolvedValue(false as never);
      const result = await authService.validateUser(mockUser.email, 'wrongPassword');

      expect(result).toBeNull();

      passwordSpy.mockRestore();
    });
  });

  describe('login', () => {
    it('should return access token if user is valid', async () => {
      const result = await authService.login({
        username: mockUser.username,
        email: mockUser.email,
      });

      expect(result.data.accessToken).toEqual('JWT_TOKEN');
      expect(result.data.username).toEqual(mockUser.username);
    });
  });

  describe('register', () => {
    it('should register the user and return true', async () => {
      const result = await authService.register({
        username: mockUser.username,
        email: mockUser.email,
        password: mockUser.password,
      });

      expect(result).toEqual(true);
    });
  });
});
