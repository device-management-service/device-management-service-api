export interface LoginDtoResponse {
  accessToken: string;
  username: string;
}
