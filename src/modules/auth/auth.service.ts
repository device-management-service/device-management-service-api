import { Injectable, Logger } from '@nestjs/common';
import { UserService } from '@modules/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { UserInfo } from 'user.type';
import { CreateUserDtoRequest } from '@modules/user/dto/create-user.dto';
import { LoginDtoResponse } from './dto/login.dto';
import { HttpResponse } from '@util/http-response';
import responseConfig from '@config/response.config';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  private readonly logger: Logger = new Logger(AuthService.name);
  constructor(private userService: UserService, private jwtService: JwtService) {}

  async validateUser(email: string, pass: string): Promise<UserInfo | null> {
    this.logger.log('auth.validateUser');
    this.logger.log(email);
    const user = await this.userService.findOne(email);
    if (!user) {
      return null;
    }

    const isValid = await bcrypt.compare(pass, user.password);
    if (isValid) {
      return {
        username: user.username,
        email: user.email,
      };
    }
    return null;
  }

  async login(user: UserInfo): Promise<HttpResponse<LoginDtoResponse>> {
    const payload = { username: user.username, email: user.email };
    return {
      statusCode: responseConfig.SUCCESS.statusCode,
      message: responseConfig.SUCCESS.message,
      data: {
        username: user.username,
        accessToken: this.jwtService.sign(payload),
      },
    };
  }

  async register(createUserDtoRequest: CreateUserDtoRequest): Promise<HttpResponse<void>> {
    return this.userService.create(createUserDtoRequest);
  }
}
