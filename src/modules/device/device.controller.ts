import { Body, Controller, Version, Request, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiOperation, ApiResponse, ApiBody, ApiTags, ApiParam, ApiBearerAuth } from '@nestjs/swagger';
import { CreateDeviceDtoRequest } from './dto/create-device.dto';
import { DeviceService } from './device.service';
import { HttpResponse } from '@util/http-response';
import { Device } from 'device.type';
import { UpdateDeviceDtoRequest } from './dto/update-device.dto';
import { CreateDeviceRequestDocument, CreateDeviceResponseDocument } from './document/create-device.document';
import { UpdateDeviceDtoRequestDocument, UpdateDeviceDtoResponseDocument } from './document/update-device.document';
import { DeviceDetailDocument } from './document/device-detail.document';

@ApiTags('Device')
@ApiBearerAuth()
@Controller('device')
export class DeviceController {
  constructor(private readonly deviceService: DeviceService) {}

  @Version('1')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Create a new device' })
  @ApiBody({
    description: 'Device creation details',
    type: CreateDeviceRequestDocument,
  })
  @ApiResponse({
    status: 201,
    description: 'Device successfully created.',
    type: CreateDeviceResponseDocument,
  })
  @Post('')
  async createDevice(
    @Body() createDeviceDto: CreateDeviceDtoRequest,
    @Request() { user },
  ): Promise<HttpResponse<void>> {
    return this.deviceService.createDevice(createDeviceDto, user.username);
  }

  @Version('1')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Fetch all devices' })
  @ApiResponse({
    status: 200,
    description: 'List of all devices.',
    type: [DeviceDetailDocument],
  })
  @Get()
  async findAll(@Request() { user }): Promise<HttpResponse<Device[]>> {
    return this.deviceService.findAll(user.username);
  }

  @Version('1')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Fetch a specific device by ID' })
  @ApiParam({ name: 'id', description: 'Device ID' })
  @ApiResponse({ status: 200, description: 'Device details', type: DeviceDetailDocument })
  @Get(':id')
  async findOne(@Param('id') id: string, @Request() { user }): Promise<HttpResponse<Device>> {
    return this.deviceService.findOne(id, user.username);
  }

  @Version('1')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Update a specific device by ID' })
  @ApiBody({ description: 'Device update details', type: UpdateDeviceDtoRequestDocument })
  @ApiParam({ name: 'id', description: 'Device ID' })
  @ApiResponse({ status: 200, description: 'Device updated successfully.', type: UpdateDeviceDtoResponseDocument })
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateDeviceDto: UpdateDeviceDtoRequest,
    @Request() { user },
  ): Promise<HttpResponse<void>> {
    return this.deviceService.update(id, updateDeviceDto, user.username);
  }

  @Version('1')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'Delete a specific device by ID' })
  @ApiParam({ name: 'id', description: 'Device ID' })
  @ApiResponse({ status: 200, description: 'Device deleted successfully.' })
  @Delete(':id')
  async delete(@Param('id') id: string, @Request() { user }): Promise<HttpResponse<void>> {
    return this.deviceService.delete(id, user.username);
  }
}
