import { ApiProperty } from '@nestjs/swagger';
import { UpdateDeviceDtoRequest } from '../dto/update-device.dto';

export class UpdateDeviceDtoRequestDocument implements UpdateDeviceDtoRequest {
  @ApiProperty({
    name: 'name',
    example: 'device-demo',
  })
  name: string;
  @ApiProperty({
    name: 'type',
    example: 'motor',
  })
  type: string;
  @ApiProperty({
    name: 'description',
    example: 'Device Demo',
  })
  description: string;
}

export class UpdateDeviceDtoResponseDocument {
  @ApiProperty({
    name: 'statusCode',
    example: 1,
  })
  statusCode: number;
  @ApiProperty({
    name: 'message',
    example: 'Success',
  })
  message: string;
}
