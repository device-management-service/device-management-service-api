import { ApiProperty } from '@nestjs/swagger';
import { CreateDeviceDtoRequest } from '../dto/create-device.dto';

export class CreateDeviceRequestDocument implements CreateDeviceDtoRequest {
  @ApiProperty({
    name: 'name',
    example: 'device-demo',
  })
  name: string;
  @ApiProperty({
    name: 'type',
    example: 'motor',
  })
  type: string;
  @ApiProperty({
    name: 'description',
    example: 'Device Demo',
  })
  description: string;
}

export class CreateDeviceResponseDocument {
  @ApiProperty({
    name: 'statusCode',
    example: 1,
  })
  statusCode: number;
  @ApiProperty({
    name: 'message',
    example: 'Success',
  })
  message: string;
}
