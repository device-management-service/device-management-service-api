import { ApiProperty } from '@nestjs/swagger';

export class DeviceDetailDocument {
  @ApiProperty({
    name: 'id',
    example: 'd1hVwL7bJmUQyps4huDFSg',
  })
  id: string;
  @ApiProperty({
    name: 'name',
    example: 'device-demo',
  })
  name: string;
  @ApiProperty({
    name: 'type',
    example: 'motor',
  })
  type: string;
  @ApiProperty({
    name: 'description',
    example: 'Device Demo',
  })
  description: string;
  @ApiProperty({
    name: 'ownby',
    example: 'user1',
  })
  ownby: string;
  @ApiProperty({
    name: 'createdAt',
    example: '2023-08-30T12:53:51.981Z',
  })
  createdAt: string;
  @ApiProperty({
    name: 'updatedAt',
    example: '2023-08-30T12:53:51.981Z',
  })
  updatedAt: string;
}
