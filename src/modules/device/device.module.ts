import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '@modules/auth/auth.module';
import { DeviceController } from './device.controller';
import { DeviceService } from './device.service';
import { DeviceDal } from './device.dal';
import { DeviceEntity, DeviceSchema } from '@schema/device.schema';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    MongooseModule.forFeature([{ name: DeviceEntity.name, schema: DeviceSchema }]),
    AuthModule,
  ],
  controllers: [DeviceController],
  providers: [DeviceService, DeviceDal],
})
export class DeviceModule {}
