import { Injectable, Logger } from '@nestjs/common';
import * as short from 'short-uuid';
import responseConfig from '@config/response.config';
import { CreateDeviceDtoRequest } from './dto/create-device.dto';
import { UpdateDeviceDtoRequest } from './dto/update-device.dto';
import { DeviceDal } from './device.dal';
import { Device } from 'device.type';
import { HttpResponse } from '@util/http-response';

@Injectable()
export class DeviceService {
  private readonly logger: Logger = new Logger(DeviceService.name);

  constructor(private deviceDal: DeviceDal) {}

  private validateRequestBody(createDeviceDto: CreateDeviceDtoRequest): boolean {
    if (!createDeviceDto.name || !createDeviceDto.type) {
      return false;
    }
    return true;
  }

  private isUsername(username: string): boolean {
    if (!username) {
      return false;
    }
    return true;
  }

  async createDevice(createDeviceDto: CreateDeviceDtoRequest, username: string): Promise<HttpResponse<void>> {
    this.logger.log('device.createDevice');
    this.logger.log(createDeviceDto);
    const date = new Date();

    if (!this.validateRequestBody(createDeviceDto)) {
      return {
        statusCode: responseConfig.INVALID_BODY.statusCode,
        message: responseConfig.INVALID_BODY.message,
      };
    }

    if (!this.isUsername(username)) {
      return {
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      };
    }

    const newDevice: Device = {
      id: short.generate(),
      name: createDeviceDto.name,
      type: createDeviceDto.type,
      description: createDeviceDto.description,
      ownby: username,
      createdAt: date,
      updatedAt: date,
    };

    try {
      await this.deviceDal.create(newDevice);
      this.logger.log(`Create device ${newDevice.name} success`);
      return {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };
    } catch (error) {
      this.logger.error('Create device error');
      this.logger.error(error);
      return {
        statusCode: responseConfig.CREATE_DEVICE_FAIL.statusCode,
        message: responseConfig.CREATE_DEVICE_FAIL.message,
      };
    }
  }

  async findAll(username: string): Promise<HttpResponse<Device[]>> {
    this.logger.log('device.findAll');
    this.logger.log(username);

    if (!this.isUsername(username)) {
      return {
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      };
    }

    const result = await this.deviceDal.findAll(username);
    return {
      statusCode: responseConfig.SUCCESS.statusCode,
      message: responseConfig.SUCCESS.message,
      data: result,
    };
  }

  async findOne(id: string, username: string): Promise<HttpResponse<Device>> {
    this.logger.log('device.findOne');
    this.logger.log(id, username);

    if (!this.isUsername(username)) {
      return {
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      };
    }

    const result = await this.deviceDal.findOne(id, username);
    if (result) {
      const data = {
        id: result.id,
        name: result.name,
        type: result.type,
        description: result.description,
        ownby: result.ownby,
        createdAt: result.createdAt,
        updatedAt: result.updatedAt,
      };

      return {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
        data,
      };
    }

    return {
      statusCode: responseConfig.DEVICE_NOT_FOUND.statusCode,
      message: responseConfig.DEVICE_NOT_FOUND.message,
    };
  }

  async update(id: string, updateDevice: UpdateDeviceDtoRequest, username: string): Promise<HttpResponse<void>> {
    this.logger.log('device.update');
    this.logger.log(username);

    if (!this.isUsername(username)) {
      return {
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      };
    }

    if (!updateDevice.description && !updateDevice.name && !updateDevice.type) {
      return {
        statusCode: responseConfig.INVALID_BODY.statusCode,
        message: responseConfig.INVALID_BODY.message,
      };
    }

    const device = await this.deviceDal.findOne(id, username);
    if (!device) {
      return {
        statusCode: responseConfig.DEVICE_NOT_FOUND.statusCode,
        message: responseConfig.DEVICE_NOT_FOUND.message,
      };
    }

    try {
      await this.deviceDal.update(id, username, updateDevice);
      this.logger.log('Update deivce success');
      return {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };
    } catch (error) {
      this.logger.error('Update device error');
      this.logger.error(error);
      return {
        statusCode: responseConfig.UPDATE_DEVICE_FAIL.statusCode,
        message: responseConfig.UPDATE_DEVICE_FAIL.message,
      };
    }
  }

  async delete(id: string, username: string): Promise<HttpResponse<void>> {
    this.logger.log('device.delete');
    this.logger.log(username);

    if (!this.isUsername(username)) {
      return {
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      };
    }

    const device = await this.deviceDal.findOne(id, username);
    if (!device) {
      return {
        statusCode: responseConfig.DEVICE_NOT_FOUND.statusCode,
        message: responseConfig.DEVICE_NOT_FOUND.message,
      };
    }

    try {
      await this.deviceDal.softDelete(id, username);
      this.logger.log('Delete deivce success');
      return {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };
    } catch (error) {
      this.logger.error('Delete device error');
      this.logger.error(error);
      return {
        statusCode: responseConfig.DELETE_DEVICE_FAIL.statusCode,
        message: responseConfig.DELETE_DEVICE_FAIL.message,
      };
    }
  }
}
