import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Device } from 'device.type';
import { DeviceEntity, DeviceDocument } from '@schema/device.schema';
import { DeviceInfo } from 'device.type';

@Injectable()
export class DeviceDal {
  constructor(@InjectModel(DeviceEntity.name) private deviceModel: Model<DeviceDocument>) {}

  async create(createDeviceDto: Device): Promise<boolean> {
    const createdDevice = new this.deviceModel(createDeviceDto);
    const result = await createdDevice.save();
    return !!result;
  }

  async findAll(ownby: string): Promise<Device[]> {
    return this.deviceModel.find({ ownby, deletedAt: { $exists: false } }).exec();
  }

  async findOne(id: string, ownby: string): Promise<Device> {
    return this.deviceModel.findOne({ id, ownby, deletedAt: { $exists: false } }).exec();
  }

  async update(id: string, ownby: string, deviceInfo: Partial<DeviceInfo>): Promise<Device> {
    const updateData = Object.entries(deviceInfo).reduce<Record<string, any>>((acc, [key, value]) => {
      acc[key] = value;
      return acc;
    }, {});
    return this.deviceModel
      .findOneAndUpdate({ id, ownby }, { ...updateData, updatedAt: new Date() }, { new: true })
      .exec();
  }

  async softDelete(id: string, ownby: string): Promise<boolean> {
    const result = await this.deviceModel
      .findOneAndUpdate({ id, ownby }, { deletedAt: new Date() }, { new: true })
      .exec();

    return !!result;
  }
}
