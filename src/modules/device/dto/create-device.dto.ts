export interface CreateDeviceDtoRequest {
  name: string;
  type: string;
  description: string;
}
