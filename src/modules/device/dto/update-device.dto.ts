import { CreateDeviceDtoRequest } from './create-device.dto';

export type UpdateDeviceDtoRequest = Partial<CreateDeviceDtoRequest>;
