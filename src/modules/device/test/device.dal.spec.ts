import { Test, TestingModule } from '@nestjs/testing';
import { DeviceDal } from '../device.dal';
import { DeviceEntity, DeviceDocument } from '@schema/device.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Device, DeviceInfo } from 'device.type';

class MockDeviceModel {
  constructor(private readonly data: DeviceInfo) {}
  save = jest.fn().mockResolvedValue(this.data);
  static find = jest.fn().mockImplementation(() => ({
    exec: jest.fn().mockResolvedValue([mockDevice]),
  }));
  static findOne = jest.fn().mockImplementation(() => ({
    exec: jest.fn().mockResolvedValue(mockDevice),
  }));
  static findOneAndUpdate = jest.fn().mockImplementation(() => ({
    exec: jest.fn().mockResolvedValue(mockDevice),
  }));
}

const mockDevice: Device = {
  id: '123',
  name: 'device1',
  type: 'type1',
  description: 'desc1',
  ownby: 'user1',
  createdAt: new Date('2023-01-01'),
  updatedAt: new Date('2023-01-01'),
};

const mockDeviceInfo: DeviceInfo = {
  name: 'device1',
  type: 'type1',
  description: 'desc1',
};

describe('DeviceDal', () => {
  let deviceDal: DeviceDal;
  let deviceModel: Model<DeviceDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DeviceDal,
        {
          provide: getModelToken(DeviceEntity.name),
          useValue: MockDeviceModel,
        },
      ],
    }).compile();

    deviceDal = module.get<DeviceDal>(DeviceDal);
    deviceModel = module.get<Model<DeviceDocument>>(getModelToken(DeviceEntity.name));
  });

  it('should be defined', () => {
    expect(deviceDal).toBeDefined();
  });

  describe('create', () => {
    it('should return true when the device is successfully created', async () => {
      const result = await deviceDal.create(mockDevice as Device);
      expect(result).toBe(true);
    });
  });

  describe('findAll', () => {
    it('should return a list of devices', async () => {
      const result = await deviceDal.findAll('user1');
      expect(result).toEqual([mockDevice]);
      expect(deviceModel.find).toHaveBeenCalledWith({ ownby: 'user1', deletedAt: { $exists: false } });
    });
  });

  describe('findOne', () => {
    it('should return a device', async () => {
      const result = await deviceDal.findOne('deviceId', 'user1');
      expect(result).toEqual(mockDevice);
    });
  });

  describe('update', () => {
    it('should return updated device when update is successful', async () => {
      const result = await deviceDal.update('123', 'user1', mockDeviceInfo);
      expect(result).toEqual(mockDevice);
    });
  });

  describe('softDelete', () => {
    it('should return true when delete is successful', async () => {
      const result = await deviceDal.softDelete('123', 'user1');
      expect(result).toBe(true);
    });
  });
});
