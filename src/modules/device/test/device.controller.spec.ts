import { Test, TestingModule } from '@nestjs/testing';
import { DeviceController } from '../device.controller';
import { DeviceService } from '../device.service';
import responseConfig from '@config/response.config';
import { Device, DeviceInfo } from 'device.type';
import { DeviceDal } from '../device.dal';

describe('DeviceController', () => {
  let deviceController: DeviceController;
  let deviceService: DeviceService;

  const mockDevice: Device = {
    id: 'id1',
    name: 'device1',
    type: 'type1',
    description: 'desc1',
    ownby: 'user1',
    createdAt: new Date('2023-01-01'),
    updatedAt: new Date('2023-01-01'),
  };

  const user = { username: 'user1' };

  const mockDeviceDto: DeviceInfo = {
    name: 'device1',
    type: 'type1',
    description: 'desc1',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DeviceController],
      providers: [
        DeviceService,
        {
          provide: DeviceDal,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            update: jest.fn(),
            softDelete: jest.fn(),
          },
        },
      ],
    }).compile();

    deviceController = module.get<DeviceController>(DeviceController);
    deviceService = module.get<DeviceService>(DeviceService);
  });

  it('should be defined', () => {
    expect(deviceController).toBeDefined();
  });

  describe('create', () => {
    it('should return a success response when a device is successfully created', async () => {
      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };

      jest.spyOn(deviceService, 'createDevice').mockResolvedValue(response);
      expect(await deviceController.createDevice(mockDeviceDto, { user })).toBe(response);
    });
  });

  describe('findAll', () => {
    it('should return a list of devices', async () => {
      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
        data: [mockDevice],
      };

      jest.spyOn(deviceService, 'findAll').mockResolvedValue(response);
      expect(await deviceController.findAll({ user })).toBe(response);
    });
  });

  describe('findOne', () => {
    it('should return a device', async () => {
      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
        data: mockDevice,
      };

      jest.spyOn(deviceService, 'findOne').mockResolvedValue(response);
      expect(await deviceController.findOne(mockDevice.id, { user })).toBe(response);
    });
  });

  describe('update', () => {
    it('should return an updated device', async () => {
      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };

      jest.spyOn(deviceService, 'update').mockResolvedValue(response);
      expect(await deviceController.update(mockDevice.id, mockDeviceDto, { user })).toBe(response);
    });
  });

  describe('delete', () => {
    it('should return a success response when a device is successfully soft deleted', async () => {
      const response = {
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      };

      jest.spyOn(deviceService, 'delete').mockResolvedValue(response);
      expect(await deviceController.delete(mockDevice.id, { user })).toBe(response);
    });
  });
});
