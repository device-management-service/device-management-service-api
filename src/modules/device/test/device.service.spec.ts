import { Test, TestingModule } from '@nestjs/testing';
import { DeviceService } from '../device.service';
import { DeviceDal } from '../device.dal';
import { CreateDeviceDtoRequest } from '../dto/create-device.dto';
import responseConfig from '@config/response.config';

describe('DeviceService', () => {
  let deviceService: DeviceService;
  let deviceDal: DeviceDal;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DeviceService,
        {
          provide: DeviceDal,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            update: jest.fn(),
            softDelete: jest.fn(),
          },
        },
      ],
    }).compile();

    deviceService = module.get<DeviceService>(DeviceService);
    deviceDal = module.get<DeviceDal>(DeviceDal);
  });

  it('should be defined', () => {
    expect(deviceService).toBeDefined();
  });

  describe('createDevice', () => {
    it('should successfully create a device', async () => {
      const dto: CreateDeviceDtoRequest = { name: 'device1', type: 'type1', description: 'desc1' };
      const username = 'user1';

      deviceDal.create = jest.fn().mockResolvedValue(true);

      const result = await deviceService.createDevice(dto, username);

      expect(deviceDal.create).toHaveBeenCalledWith(
        expect.objectContaining({
          description: 'desc1',
          name: 'device1',
          ownby: 'user1',
          type: 'type1',
        }),
      );
      expect(result).toEqual({
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      });
    });

    it('should return an error when called with invalid arguments', async () => {
      const dto: CreateDeviceDtoRequest = { name: '', type: '', description: '' }; // invalid arguments
      const username = 'user1';

      const result = await deviceService.createDevice(dto, username);

      expect(deviceDal.create).not.toHaveBeenCalled();

      expect(result).toEqual({
        statusCode: responseConfig.INVALID_BODY.statusCode,
        message: responseConfig.INVALID_BODY.message,
      });
    });

    it('should return an error response when device creation fails', async () => {
      const dto: CreateDeviceDtoRequest = { name: 'device1', type: 'type1', description: 'desc1' };
      const username = 'user1';

      deviceDal.create = jest.fn().mockImplementation(() => {
        throw new Error('Device creation failed');
      });

      const result = await deviceService.createDevice(dto, username);

      expect(deviceDal.create).toHaveBeenCalledWith(
        expect.objectContaining({
          description: 'desc1',
          name: 'device1',
          ownby: 'user1',
          type: 'type1',
        }),
      );

      expect(result).toEqual({
        statusCode: responseConfig.CREATE_DEVICE_FAIL.statusCode,
        message: responseConfig.CREATE_DEVICE_FAIL.message,
      });
    });

    it('should return an error response when user is not given', async () => {
      const dto: CreateDeviceDtoRequest = { name: 'device1', type: 'type1', description: 'desc1' };
      const username = '';

      const result = await deviceService.createDevice(dto, username);

      expect(deviceDal.create).not.toHaveBeenCalled();

      expect(result).toEqual({
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      });
    });
  });

  describe('findAll', () => {
    it('should return all devices for a given user', async () => {
      const username = 'user1';
      const mockDevices = [
        { id: 'id1', name: 'device1', type: 'type1', description: 'desc1', ownby: 'user1' },
        { id: 'id2', name: 'device2', type: 'type2', description: 'desc2', ownby: 'user1' },
      ];

      deviceDal.findAll = jest.fn().mockResolvedValue(mockDevices);

      const result = await deviceService.findAll(username);

      expect(deviceDal.findAll).toHaveBeenCalledWith(username);
      expect(result).toEqual({
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
        data: mockDevices,
      });
    });

    it('should return an error response when user is not given', async () => {
      const username = '';

      const result = await deviceService.findAll(username);

      expect(deviceDal.findAll).not.toHaveBeenCalled();

      expect(result).toEqual({
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      });
    });
  });

  describe('findOne', () => {
    it('should return the device for a given id and username', async () => {
      const id = 'id1';
      const username = 'user1';
      const mockDevice = { id: 'id1', name: 'device1', type: 'type1', description: 'desc1', ownby: 'user1' };

      deviceDal.findOne = jest.fn().mockResolvedValue(mockDevice);

      const result = await deviceService.findOne(id, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(result).toEqual({
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
        data: mockDevice,
      });
    });

    it('should return an error response when user is not given', async () => {
      const id = 'id1';
      const username = '';

      const result = await deviceService.findOne(id, username);

      expect(deviceDal.findOne).not.toHaveBeenCalled();

      expect(result).toEqual({
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      });
    });

    it('should return DEVICE_NOT_FOUND when no device is found for the given id and username', async () => {
      const id = 'id1';
      const username = 'user1';

      deviceDal.findOne = jest.fn().mockResolvedValue(null);

      const result = await deviceService.findOne(id, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(result).toEqual({
        statusCode: responseConfig.DEVICE_NOT_FOUND.statusCode,
        message: responseConfig.DEVICE_NOT_FOUND.message,
      });
    });
  });

  describe('update', () => {
    it('should successfully update a device', async () => {
      const id = 'id1';
      const username = 'user1';
      const updateDeviceDto = { name: 'updatedDevice1', type: 'updatedType1', description: 'updatedDesc1' };
      const mockDevice = { id: 'id1', name: 'device1', type: 'type1', description: 'desc1', ownby: 'user1' };

      deviceDal.findOne = jest.fn().mockResolvedValue(mockDevice);
      deviceDal.update = jest.fn().mockResolvedValue(true);

      const result = await deviceService.update(id, updateDeviceDto, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(deviceDal.update).toHaveBeenCalledWith(id, username, updateDeviceDto);
      expect(result).toEqual({
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      });
    });

    it('should return an error response when the update operation fails', async () => {
      const id = 'id1';
      const username = 'user1';
      const updateDeviceDto = { name: 'updatedDevice1', type: 'updatedType1', description: 'updatedDesc1' };
      const mockDevice = { id: 'id1', name: 'device1', type: 'type1', description: 'desc1', ownby: 'user1' };

      deviceDal.findOne = jest.fn().mockResolvedValue(mockDevice);
      deviceDal.update = jest.fn().mockImplementation(() => {
        throw new Error('Update operation failed');
      });

      const result = await deviceService.update(id, updateDeviceDto, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(deviceDal.update).toHaveBeenCalledWith(id, username, updateDeviceDto);
      expect(result).toEqual({
        statusCode: responseConfig.UPDATE_DEVICE_FAIL.statusCode,
        message: responseConfig.UPDATE_DEVICE_FAIL.message,
      });
    });

    it('should return an error response when send invalid body', async () => {
      const id = 'id1';
      const username = 'user1';
      const updateDeviceDto = { name: '', type: '', description: '' };

      const result = await deviceService.update(id, updateDeviceDto, username);

      expect(deviceDal.findOne).not.toHaveBeenCalled();
      expect(deviceDal.update).not.toHaveBeenCalled();
      expect(result).toEqual({
        statusCode: responseConfig.INVALID_BODY.statusCode,
        message: responseConfig.INVALID_BODY.message,
      });
    });

    it('should return DEVICE_NOT_FOUND when no device is found for the given id and username', async () => {
      const id = 'id1';
      const username = 'user1';
      const updateDeviceDto = { name: 'updatedDevice1', type: 'updatedType1', description: 'updatedDesc1' };

      deviceDal.findOne = jest.fn().mockResolvedValue(null);

      const result = await deviceService.update(id, updateDeviceDto, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(result).toEqual({
        statusCode: responseConfig.DEVICE_NOT_FOUND.statusCode,
        message: responseConfig.DEVICE_NOT_FOUND.message,
      });
    });

    it('should return an error response when user is not given', async () => {
      const id = 'id1';
      const username = '';
      const updateDeviceDto = { name: 'updatedDevice1', type: 'updatedType1', description: 'updatedDesc1' };

      const result = await deviceService.update(id, updateDeviceDto, username);

      expect(deviceDal.findOne).not.toHaveBeenCalled();
      expect(deviceDal.update).not.toHaveBeenCalled();

      expect(result).toEqual({
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      });
    });
  });

  describe('delete', () => {
    it('should successfully delete a device', async () => {
      const id = 'id1';
      const username = 'user1';
      const mockDevice = { id: 'id1', name: 'device1', type: 'type1', description: 'desc1', ownby: 'user1' };

      deviceDal.findOne = jest.fn().mockResolvedValue(mockDevice);
      deviceDal.softDelete = jest.fn().mockResolvedValue(true);

      const result = await deviceService.delete(id, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(deviceDal.softDelete).toHaveBeenCalledWith(id, username);
      expect(result).toEqual({
        statusCode: responseConfig.SUCCESS.statusCode,
        message: responseConfig.SUCCESS.message,
      });
    });

    it('should return an error response when the delete operation fails', async () => {
      const id = 'id1';
      const username = 'user1';
      const mockDevice = { id: 'id1', name: 'device1', type: 'type1', description: 'desc1', ownby: 'user1' };

      deviceDal.findOne = jest.fn().mockResolvedValue(mockDevice);
      deviceDal.softDelete = jest.fn().mockImplementation(() => {
        throw new Error('Delete operation failed');
      });

      const result = await deviceService.delete(id, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(deviceDal.softDelete).toHaveBeenCalledWith(id, username);
      expect(result).toEqual({
        statusCode: responseConfig.DELETE_DEVICE_FAIL.statusCode,
        message: responseConfig.DELETE_DEVICE_FAIL.message,
      });
    });

    it('should return an error response when user is not given', async () => {
      const id = 'id1';
      const username = '';

      const result = await deviceService.delete(id, username);

      expect(deviceDal.findOne).not.toHaveBeenCalled();
      expect(deviceDal.softDelete).not.toHaveBeenCalled();

      expect(result).toEqual({
        statusCode: responseConfig.USER_NOT_FOUND.statusCode,
        message: responseConfig.USER_NOT_FOUND.message,
      });
    });

    it('should return DEVICE_NOT_FOUND when no device is found for the given id and username', async () => {
      const id = 'id1';
      const username = 'user1';

      deviceDal.findOne = jest.fn().mockResolvedValue(null);

      const result = await deviceService.delete(id, username);

      expect(deviceDal.findOne).toHaveBeenCalledWith(id, username);
      expect(result).toEqual({
        statusCode: responseConfig.DEVICE_NOT_FOUND.statusCode,
        message: responseConfig.DEVICE_NOT_FOUND.message,
      });
    });
  });
});
