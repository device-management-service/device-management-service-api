import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { DeviceModule } from '@modules/device/device.module';
import { UserModule } from '@modules/user/user.module';
import { AuthModule } from '@modules/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get<string>('DB_URI'),
        dbName: configService.get<string>('DB_NAME'),
        retryWrites: true,
        w: 'majority',
        user: configService.get<string>('DB_USER'),
        pass: configService.get<string>('DB_PASSWORD'),
      }),
      inject: [ConfigService],
    }),
    AuthModule,
    DeviceModule,
    UserModule,
  ],
})
export class AppModule {}
