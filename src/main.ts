import { NestFactory } from '@nestjs/core';
import 'reflect-metadata';
import { VersioningType } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const port = process.env.SERVER_PORT || 3000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['debug', 'log', 'error', 'warn'],
  });
  app.setGlobalPrefix('/api');
  app.enableVersioning({
    type: VersioningType.URI,
  });

  // WANING: This is a workaround to handle cors. Do Not enable on production!!
  if (process.env.NODE_ENV === 'development') {
    app.enableCors();
  }

  const config = new DocumentBuilder()
    .setTitle('Device Management Service Api')
    .setDescription('Device Management Service Api')
    .addBearerAuth()
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/doc', app, document);

  await app.listen(port);
}

bootstrap();
