export default {
  SUCCESS: {
    statusCode: 1,
    message: 'Success',
  },
  USER_ALREADY_EXIST: {
    statusCode: 101,
    message: 'User Already Exist',
  },
  CANNOT_CREATE_USER: {
    statusCode: 102,
    message: 'Cannot Create User',
  },
  CREATE_DEVICE_FAIL: {
    statusCode: 103,
    message: 'Create Device Fail',
  },
  DEVICE_NOT_FOUND: {
    statusCode: 104,
    message: 'Device Not Found',
  },
  UPDATE_DEVICE_FAIL: {
    statusCode: 105,
    message: 'Update Device Fail',
  },
  DELETE_DEVICE_FAIL: {
    statusCode: 106,
    message: 'Delete Device Fail',
  },
  USER_NOT_FOUND: {
    statusCode: 107,
    message: 'User not found',
  },
  INVALID_BODY: {
    statusCode: 400,
    message: 'Invalid Body Parameter',
  },
};
