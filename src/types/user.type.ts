export interface User {
  username: string;
  email: string;
  password: string;
  createdAt: Date;
}

export interface UserInfo {
  username: string;
  email: string;
}
