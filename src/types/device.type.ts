export interface Device {
  id: string;
  name: string;
  type: string;
  description: string;
  ownby: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}

export interface DeviceInfo {
  name: string;
  type: string;
  description: string;
}
