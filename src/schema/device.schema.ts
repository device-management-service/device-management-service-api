import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { DEVICE_COLLECTION_NAME } from '@config/database.config';

export type DeviceDocument = HydratedDocument<DeviceEntity>;

@Schema({ collection: DEVICE_COLLECTION_NAME })
export class DeviceEntity {
  @Prop({ type: String, name: 'id', unique: true, index: true })
  id: string;

  @Prop({ type: String, name: 'name', required: true })
  name: string;

  @Prop({ type: String, name: 'type', required: true })
  type: string;

  @Prop({ type: String, name: 'description' })
  description: string;

  @Prop({ type: String, name: 'ownby' })
  ownby: string;

  @Prop({ type: Date, name: 'createdAt', default: Date.now })
  createdAt: Date;

  @Prop({ type: Date, name: 'updatedAt', default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, name: 'deletedAt' })
  deletedAt: Date;
}

export const DeviceSchema = SchemaFactory.createForClass(DeviceEntity);
