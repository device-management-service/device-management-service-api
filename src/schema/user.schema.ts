import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { USER_COLLECTION_NAME } from '@config/database.config';

export type UserDocument = HydratedDocument<UserEntity>;

@Schema({ collection: USER_COLLECTION_NAME })
export class UserEntity {
  @Prop({ name: 'username', type: String, required: true })
  username: string;

  @Prop({
    name: 'email',
    type: String,
    required: true,
    unique: true,
    index: true,
  })
  email: string;

  @Prop({ name: 'password', type: String, required: true })
  password: string; // Hash password

  @Prop({ name: 'createdAt', type: Date, default: Date.now })
  createdAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
